" vim: foldmethod=marker

" {{{ Plugins
call plug#begin('~/.local/share/nvim/plugged')

" File types
Plug 'Vimjas/vim-python-pep8-indent', {'for': 'python'}
Plug 'cespare/vim-toml', {'for': 'toml'}
Plug 'jceb/vim-orgmode', {'for': 'org'}
Plug 'ledger/vim-ledger', {'for': 'ledger'}
Plug 'rust-lang/rust.vim', {'for': 'rust'}
Plug 'HerringtonDarkholme/yats.vim', {'for': 'typescript'}

" Editing
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'

" UI
Plug 'ctrlpvim/ctrlp.vim', {'on': ['CtrlP', 'CtrlPBuffer']}
Plug 'nathanaelkane/vim-indent-guides'
Plug 'scrooloose/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'nanotech/jellybeans.vim'

call plug#end()

let g:NERDTreeHijackNetrw     = 1
let g:NERDTreeMapActivateNode = '<space>'
let g:NERDTreeMinimalUI       = 1
let g:ledger_date_format      = '%d.%m.%Y'
let g:ledger_maxwidth         = 80
let g:org_indent              = 1
let g:ctrlp_working_path_mode = 'r'  " Work relative to VCS root

" Use VCS file listing
let g:ctrlp_user_command = {
      \ 'types': {
        \ 1: ['.git', 'git -C %s ls-files -co --exclude-standard'],
        \ 2: ['.hg', 'hg --cwd %s locate -I .'],
      \ },
        \ 'fallback': 'find %s -type f'
      \ }
" }}}

" {{{ Configuration
syntax on
set ruler showcmd
set softtabstop=4 shiftwidth=4 expandtab colorcolumn=80
set autoindent
set listchars=tab:→\ ,space:.,eol:$,extends:>,trail:-
set exrc secure
set mouse=a
set undofile
set notimeout ttimeout
set nowrap
set scrolloff=4

set clipboard+=unnamedplus

" Do not store global and local values or folds in sessions
set ssop-=options
set ssop-=folds

" Let { and } skip over folds instead of opening them
set foldopen-=block

if !exists("g:colors_name") && empty($VT420)
  set termguicolors
  colors jellybeans
endif

if empty($SSH_AUTH_SOCK)
  let $SSH_AUTH_SOCK = system("gpgconf --list-dir agent-ssh-socket")
endif

" (Gentoo) Make vanilla-vim vimfiles available in neovim
if !empty(glob("/usr/share/vim/vimfiles"))
  set rtp+=/usr/share/vim/vimfiles
endif
" }}}

" {{{ Autocommands

" Make focus events cause autowrites.
set autoread autowrite
au! FocusLost * silent! wa

" Auto-reload init.vim
au! BufWritePost $MYVIMRC source $MYVIMRC

" Equalize splits on resize
au! VimResized * wincmd =

" }}}

" {{{ Bindings
nmap <silent> <leader>e :e.<CR>
nmap <silent> <leader>l :set list!<CR>
nmap <silent> <leader>n :noh<CR>
vmap          <leader>s y:%s,\<<C-R>"\>,,gc<Left><Left><Left>

nmap <silent> <C-p> :CtrlP<CR>
nmap <silent> <leader>d :CtrlPBuffer<CR>

nmap <leader>t/ :Tabularize /
nmap <leader>t= :Tabularize /=<CR>
nmap <leader>t: :Tabularize /:<CR>
nmap <leader>t\ :Tabularize /\<CR>
nmap <leader>tt :Tabularize<CR>

nmap <silent> <leader>ag :!open -a GitUp .<CR><CR>
nmap <silent> <leader>at :!open -a iTerm .<CR><CR>

" Opposite of J
nnoremap S ht lr<CR>k

tnoremap <Esc> <C-\><C-n>

" For comfortable \-typing on Mac keyboards
nmap ¿ \
imap ¿ \

" }}}

" {{{ Highlight trailing whitespace
if &ro != "1"
  highlight extrawhitespace ctermbg=red guibg=red
  match extrawhitespace /\s\+$/
  au! bufwinenter * match extrawhitespace /\s\+$/
  au! insertenter * match extrawhitespace /\s\+\%#\@<!$/
  au! insertleave * match extrawhitespace /\s\+$/
  au! bufwinleave * call clearmatches()
end
" }}}
